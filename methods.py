from py_expression_eval import Parser

parser = Parser()


class BaseMethod:

    k = None
    k0 = None
    fx = None
    Ea = None
    stop = False
    previous = None
    variables = None
    tolerance = 0.001

    NON_VALUE = '---'

    def __input__(self, other_args=None):
        self.fx = parser.parse(str(input('What is the function: ')))
        self.variables = self.fx.variables()
        print('Variables: {} \n'.format(self.variables))
        
        # TODO: Validate variables lenght = 1

        if other_args:
            other_args()

        tolerance = input('What is the tolerance (default = 0.001): ')

        if tolerance:
            self.tolerance = parser.parse(str(tolerance)).evaluate({})

        print('Tolerance: {} \n'.format(self.tolerance))

    def _calculate_error(self, xk):
        if self.k > self.k0:
            self.Ea = parser.parse('abs(A - a)/abs(A)').evaluate({'A': xk, 'a': self.previous})
            self.stop = self.Ea < self.tolerance

    def _eval_fx(self, xk):
        return self.fx.evaluate({self.variables[0]: xk})

    def _get_error(self):
        return self.Ea if self.Ea else self.NON_VALUE


class OpenedMethod(BaseMethod):

    X0 = None
    X1 = None

    def _start(self):
        self.__input__()
        self._eval()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.k = 0
        self.k0 = 0

    def __input__(self):
        super().__input__(self._get_seeds)

    def _get_seeds(self):
        self.X0 = float(eval(input('[Seeds] X0: ')))
        self.X1 = float(eval(input('[Seeds] X1: ')))

        print('Seeds: [X0 = {}] [X1 = {}] \n'.format(self.X0, self.X1))

    def _print_headers(self):
        print(' k | xk | f(xk) | Ea \n')

    def _print_row(self, xk, fxk):
        print(' {} | {} | {} | {} '.format(self.k, xk, fxk, self._get_error()))

    def _eval(self):
        x0 = self.X0
        x1 = self.X1
        f0 = self._eval_fx(x0)
        f1 = self._eval_fx(x1)
        self._print_headers()
        self._print_row(x0, f0)
        self.previous = x0
        self.k += 1
        self._calculate_error(x1)
        self._print_row(x1, f1)
        self.previous = x1

        while not self.stop:
            self.k += 1
            xk = x1 - ((f1 * (x1 - x0))/(f1 - f0))
            fxk = self._eval_fx(xk)
            self._calculate_error(xk)
            self._print_row(xk, fxk)
            self.previous = xk
            
            x0 = x1
            f0 = f1
            x1 = xk
            f1 = fxk

        print('\n The approximation is: {}'.format(xk))


class ClosedMethod(BaseMethod):

    a = None
    b = None

    def _start(self, get_values):
        self.__input__()
        self._eval(get_values)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.k = 1
        self.k0 = 1

    def __input__(self):
        super().__input__(self._get_interval)

    def _get_interval(self):
        self.a = float(eval(input('[Interval] from: ')))
        self.b = float(eval(input('[Interval] to: ')))

        # TODO: Validate interval (b > a)

        print('Interval: [{}, {}] \n'.format(self.a, self.b))

    def _evaluate_points(self, a, b, xk=None):
        fa = self._eval_fx(a)
        fb = self._eval_fx(b)

        if xk:
            return fa, fb, self._eval_fx(xk)

        return fa, fb

    def _print_headers(self):
        print(' k | a | xk | b | f(a) | f(xk) | f(b) | Ea \n')

    def _print_row(self, a, xk, b, fa, fxk, fb):
        print(' {} | {} | {} | {} | {} | {} | {} | {} '.format(
            self.k, a, xk, b, fa, fxk, fb, self._get_error())
        )

    def _eval(self, get_values):
        a = self.a
        b = self.b
        self._print_headers()

        while not self.stop:
            xk, fa, fb, fxk = get_values(self, a, b)
            self._calculate_error(xk)
            self.previous = xk
            self._print_row(a, xk, b, fa, fxk, fb)
            self.k += 1

            if fa * fxk > 0:
                a = xk
            else:
                b = xk
        
        print('\n The approximation is: {}'.format(xk))
        

# x^3 + 2*x^2 - 4*x + 3

class Bisection(ClosedMethod):

    @classmethod
    def eval(cls):
        instance = Bisection()._start(cls.get_values)

    def get_values(self, a, b):
        xk = (a + b) / 2
        fa, fb, fxk = self._evaluate_points(a, b, xk)
        return xk, fa, fb, fxk


class FakePosition(ClosedMethod):

    @classmethod
    def eval(cls):
        instance = FakePosition()._start(cls.get_values)

    def get_values(self, a, b):
        fa, fb = self._evaluate_points(a, b)
        xk = ((a * fb) - (b * fa))/(fb - fa)
        return xk, fa, fb, self._eval_fx(xk)


class NewtonRaphson:
    pass

# E^(-x^2) - x

class SecantLine(OpenedMethod):

    @classmethod
    def eval(cls):
        instance = SecantLine()._start()

# Bisection.eval()
# FakePosition.eval()
SecantLine.eval()